define(['angular'], function(angular) {

    var ngModule = angular.module('cockpit.plugin.cockpit-plugin', []);

    var DashboardController = function($scope, $http, Uri) {

        // Updates servers by querying server
        $scope.updateServers = function() {
            $http
                .get(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server"))
                .success(function(data) {
                    $scope.silaServerList = data;
                })
        };
        setInterval($scope.updateServers, 2000);

        // Adds server
        $scope.addServer = function(newHost, newPort)
        {
            $http
                .post(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server"), {
                    "host": newHost,
                    "port": newPort
                })
                .success(function(data) {
                    $http
                        .get(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server"))
                        .success(function(data) {
                            $scope.silaServerList = data;
                        })
                });
        };

        // Delete server
        $scope.deleteServer = function(identifier) {
            var data = $.param({
                "identifier": identifier
            });
            $http
                .delete(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server") + "?" + data)
                .success(function(data) {
                    $http
                        .get(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server"))
                        .success(function(data) {
                            $scope.silaServerList = data;
                        })
                });
            $scope.serverToDelete = null;
        }

        // Scan network
        $scope.scanNetwork = function(){
            $http
                .post(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server/scanNetwork"))
                .success(function(data) {
                    $scope.message = data;
                });
        };

        // Modeler Template
        $scope.getModelerTemplate = function(key, name) {
            var data = $.param({
                "identifier": key
            });
            $http
                .get(Uri.appUri("plugin://cockpit-plugin/:engine/sila-server/modelerTemplate") + "?" + data)
                .success(function(data) {
                    var blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
                    var a = $("<a style='display: none;'/>");
                    var url= window.URL.createObjectURL(blob);
                    a.attr("href", url);
                    a.attr("download", name);
                    $("body").append(a);
                    a[0].click();
                    window.URL.revokeObjectURL(url);
                    a.remove();
                })
        };
    };

    DashboardController.$inject = ["$scope", "$http", "Uri"];

    var Configuration = function Configuration(ViewsProvider) {

        ViewsProvider.registerDefaultView('cockpit.dashboard', {
            id: 'cockpit-plugin',
            label: 'SiLA Cockpit Plugin',
            url: 'plugin://cockpit-plugin/static/app/dashboard.html',
            controller: DashboardController,
            // make sure we have a higher priority than the default plugin
            priority: 12
        });
    };

    Configuration.$inject = ['ViewsProvider'];

    ngModule.config(Configuration);
    ngModule.controller('DashboardController', [ '$scope' ]);

    return ngModule;
});
