package sila_camunda.cockpit_plugin.resources;

import java.util.Map;
import java.util.UUID;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.cockpit.plugin.resource.AbstractCockpitPluginResource;

import sila_camunda.cockpit_plugin.dto.AddServerBody;
import sila_camunda.core.modeler.CamundaModelerTemplate;
import sila_java.library.manager.ServerAdditionException;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;

@Slf4j
public class SiLAServerResource extends AbstractCockpitPluginResource {
    SiLAServerResource(String engineName) {
        super(engineName);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<UUID, Server> getSiLAServerList() {
        return ServerManager.getInstance().getServers();
    }

    @POST
    @Produces("text/plain")
    public String addServer(AddServerBody addServerBody) throws ServerAdditionException {
        log.info("Post Request received: " + addServerBody.toString());
        ServerManager.getInstance().addServer(addServerBody.getHost(), addServerBody.getPort());
        return "done";
    }

    @DELETE
    @Produces("text/plain")
    public String removeServer(@QueryParam("identifier") String identifier) {
        log.info("delete Request received: " + identifier);
        ServerManager.getInstance().removeServer(UUID.fromString(identifier));
        return "done";
    }

    @Path("scanNetwork")
    @POST
    @Produces("text/plain")
    public String scanNetwork() {
        log.info("Network scan Request received");
        ServerManager.getInstance().getDiscovery().scanNetwork();
        return "done";
    }

    @Path("modelerTemplate")
    @GET
    @Produces("text/plain")
    public String getModelerTemplate(@QueryParam("identifier") String identifier) {
        final Gson gson = new GsonBuilder().create();
        final Server server = ServerManager
                .getInstance()
                .getServers()
                .get(UUID.fromString(identifier));

        final String modelerTemplate = gson.toJson(CamundaModelerTemplate.createModelerTemplate(
                server
        ));

        // Note: In Json representation, string booleans should be "normal" booleans
        return modelerTemplate
                .replaceAll("\"true\"", "true")
                .replaceAll("\"false\"", "false");
    }
}
