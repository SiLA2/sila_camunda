# Spring Boot Web application with Camunda Webapps

- Standalone web application with a simple example `hello_sila.bpmn` process deployed.
- For this to work run the `hello_sila` server of `sila_java`. Add the server manually or
enable SiLA Discovery.
- To deploy new process definitions from modeler, configure the endpoint to
`http://localhost:8080/rest/engine/default/deployment/create`. Caution: the camunda modeler
is pretty buggy and won't show if the deployment failed.
- To use the Element templates, download them in the respective folder of the Modeler (see 
[reference](https://github.com/camunda/camunda-modeler/tree/master/docs/element-templates#example-setup))
- Another example uses the two commonly used instruments bioshake and multidrop
(`open_servers.bpmn`) for which the SiLA Servers are also found in `sila_java`.

## Run the application and use Camunda Webapps

You can build the application with `mvn clean install` and then run it with `java -jar` command.

Then you can access Camunda Webapps in browser: `http://localhost:8080` (provide login/password from `application.yaml`)

