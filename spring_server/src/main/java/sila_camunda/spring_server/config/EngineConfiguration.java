package sila_camunda.spring_server.config;

import lombok.val;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * Changing Text Variables to be stored as blobs to allow big JSON payloads
 */
@Component
@Order(Ordering.DEFAULT_ORDER + 1)
public class EngineConfiguration implements ProcessEnginePlugin {
    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {}

    @Override
    public void postInit(ProcessEngineConfigurationImpl processEngineConfiguration) {}

    @Override
    public void postProcessEngineBuild(ProcessEngine processEngine) {
        try {
            val processEngineConfiguration = (ProcessEngineConfigurationImpl) processEngine
                    .getProcessEngineConfiguration();
            val session = processEngineConfiguration
                    .getDbSqlSessionFactory().getSqlSessionFactory().openSession();
            val connection = session.getConnection();
            val jdbcStatement = connection.createStatement();
            jdbcStatement.execute("ALTER TABLE ACT_RU_VARIABLE ALTER TEXT_ CLOB");
            jdbcStatement.execute("ALTER TABLE ACT_HI_VARINST ALTER TEXT_ CLOB");
            jdbcStatement.execute("ALTER TABLE ACT_HI_DETAIL ALTER TEXT_ CLOB");
            jdbcStatement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
