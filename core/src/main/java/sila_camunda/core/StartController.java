package sila_camunda.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila_camunda.core.db.ServerPersistence;
import sila_camunda.core.db.impl.BinaryPersistence;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Start controller loading and saving persistence layer at startup / shutdown
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class StartController {
    public static void init() {
        // Note: Configuration can switch between implementations here
        ServerPersistence serverPersistence = new BinaryPersistence();

        ServerManager.getInstance().initialize(serverPersistence.getServers());

        Runtime.getRuntime().addShutdownHook(
                new Thread(()->saveSiLAServers(serverPersistence))
        );
    }

    private static void saveSiLAServers(ServerPersistence serverPersistence) {
        final Map<UUID, Server> serverMap = new HashMap<>(ServerManager.getInstance().getServers());
        serverMap.values().forEach(server -> server.setStatus(Server.Status.OFFLINE));

        serverPersistence.saveServers(serverMap);
    }
}
