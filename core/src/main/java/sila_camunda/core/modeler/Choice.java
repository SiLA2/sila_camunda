package sila_camunda.core.modeler;

import lombok.Value;

@Value
public class Choice {
    private String name;
    private String value;
}
