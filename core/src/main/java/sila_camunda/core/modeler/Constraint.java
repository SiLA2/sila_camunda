package sila_camunda.core.modeler;

import lombok.Value;

/**
 * Currently there is only nonEmpty constraints
 */
@Value
public class Constraint {
    private final boolean notEmpty = true;
}
