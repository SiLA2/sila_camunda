package sila_camunda.core.db.impl;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import lombok.extern.slf4j.Slf4j;
import org.objenesis.strategy.StdInstantiatorStrategy;
import sila_camunda.core.db.ServerPersistence;
import sila_java.library.manager.models.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class BinaryPersistence implements ServerPersistence {
    private static final String filePath = System.getProperty("user.home")
            + File.separator + ".sila"
            + File.separator + "camunda"
            + File.separator + "sila_servers.bin";

    private final Kryo kryo = new Kryo();
    private final MapSerializer mapSerializer = new MapSerializer();

    public BinaryPersistence() {
        // For missing no-arg constructors
        kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
        // Automatically register "sub classes"
        kryo.setRegistrationRequired(false);

        kryo.register(UUID.class);
        kryo.register(Server.class);
        kryo.register(HashMap.class, mapSerializer);

        mapSerializer.setKeyClass(UUID.class, kryo.getSerializer(UUID.class));
        mapSerializer.setValueClass(Server.class, kryo.getSerializer(Server.class));

        // Create Parent Directories
        new File(filePath).getParentFile().mkdirs();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<UUID, Server> getServers() {
        try (final Input input = new Input(new FileInputStream(filePath))) {
            return kryo.readObject(input, HashMap.class);
        } catch (FileNotFoundException | KryoException e) {
            log.info("Binary persistence found at: " + filePath + " could not be loaded blank state assumed.");
            log.info("Reason: " + e.getMessage());
            return new HashMap<>();
        }
    }

    @Override
    public void saveServers(Map<UUID, Server> serverMap) {
        log.info("Persisting Server state of " + serverMap.size() + " servers.");
        try (final Output output =
                new Output(new FileOutputStream(filePath))) {
            final HashMap<UUID, Server> serverHashMap = new HashMap<>();
            serverHashMap.putAll(serverMap);
            kryo.writeObject(output, serverHashMap);
        } catch (FileNotFoundException | KryoException e) {
            log.warn("Binary Persistence could not save the Server state: " + e.getMessage());
        }
    }
}
