package sila_camunda.core.db;

import sila_java.library.manager.models.Server;

import java.util.Map;
import java.util.UUID;

/**
 * Persisting Server Map over Application Lifecycles
 */
public interface ServerPersistence {
    Map<UUID, Server> getServers();
    void saveServers(Map<UUID, Server> serverMap);
}
